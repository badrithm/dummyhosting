// THM api code
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const getHomes = async (source,handle) => {
  console.log('Getting homes.....')
  if (!source || !handle) {
    throw new Error('Source and handle required.')
  }

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmM2NDYwNGZkYzY4NjVjMWI4ZWIwYiIsIm5hbWUiOiJDb3ZlJ3MgZnJvbnRhcHAgdG9rZW4iLCJ1dWlkIjoiOGQ4ZWQzOTAtNTQxYi0xMWViLThmZmMtMTE0ZmYyZGEwNGJkIiwib3JnYW5pemF0aW9uIjoiNWU5ODBmM2ZkMGZmZWU3Zjc5MjIwMjBiIiwidWlkIjoiNWZjODg0OWE2N2I2ZDAzMTdmMTc4ZWY4IiwiaWF0IjoxNjE4NDA2OTcxLCJleHAiOjE2MjYxODI5NzF9.ZkOWHLwRNAC1hS3VKeU9JKC5lDF4r367MYoXTbMcH9Y");
  myHeaders.append("x-api-key", "8d8ed390-541b-11eb-8ffc-114ff2da04bd");
  myHeaders.append("Content-Type", "application/json");
  myHeaders.append("Origin", "https://app.frontapp.com");
  myHeaders.append("Content-Security-Policy", "frame-ancestors https://*.frontapp.com https://*.frontapplication.com");
  var raw = JSON.stringify({"source":source,"handle":handle});
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };
  return fetch("https://cors-anywhere.herokuapp.com/https://rentals.thehousemonk.com/integration/frontapp/homes", requestOptions)
    .then(response => {
      console.log(response)
      return response.json()
    })
    .then(result => result)
    .catch(error => {
      throw error
    });
}

const getTickets = async (homeId) => {
  console.log('Getting tickets.....')
  if (!homeId) {
    throw new Error('Home ID required.')
  }

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmM2NDYwNGZkYzY4NjVjMWI4ZWIwYiIsIm5hbWUiOiJDb3ZlJ3MgZnJvbnRhcHAgdG9rZW4iLCJ1dWlkIjoiOGQ4ZWQzOTAtNTQxYi0xMWViLThmZmMtMTE0ZmYyZGEwNGJkIiwib3JnYW5pemF0aW9uIjoiNWU5ODBmM2ZkMGZmZWU3Zjc5MjIwMjBiIiwidWlkIjoiNWZjODg0OWE2N2I2ZDAzMTdmMTc4ZWY4IiwiaWF0IjoxNjE4NDA2OTcxLCJleHAiOjE2MjYxODI5NzF9.ZkOWHLwRNAC1hS3VKeU9JKC5lDF4r367MYoXTbMcH9Y");
  myHeaders.append("x-api-key", "8d8ed390-541b-11eb-8ffc-114ff2da04bd");
  myHeaders.append("Origin", "https://app.frontapp.com");
  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };
  return fetch("https://cors-anywhere.herokuapp.com/https://rentals.thehousemonk.com/integration/frontapp/tickets?home="+homeId+"&limit=30&skip=0", requestOptions)
    .then(response => {
      console.log(response)
      return response.json()
    })
    .then(result => {
      if (result.data) {
        if (Array.isArray(result.data)) {
          return result
        } else {
          throw new Error ('Error in categories data.')
        } 
      } else {
        throw new Error ('Error in categories data.')
      }
    })
    .catch(error => {
      throw error
    });
}

const getSignedUrl = async (data) => {
  console.log('Uploading file.....')
  console.log(data)
  if (!data.fileName) {
    throw new Error('Home ID required.')
  }
  var myHeaders = new Headers();
  myHeaders.append("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmM2NDYwNGZkYzY4NjVjMWI4ZWIwYiIsIm5hbWUiOiJDb3ZlJ3MgZnJvbnRhcHAgdG9rZW4iLCJ1dWlkIjoiOGQ4ZWQzOTAtNTQxYi0xMWViLThmZmMtMTE0ZmYyZGEwNGJkIiwib3JnYW5pemF0aW9uIjoiNWU5ODBmM2ZkMGZmZWU3Zjc5MjIwMjBiIiwidWlkIjoiNWZjODg0OWE2N2I2ZDAzMTdmMTc4ZWY4IiwiaWF0IjoxNjE4NDA2OTcxLCJleHAiOjE2MjYxODI5NzF9.ZkOWHLwRNAC1hS3VKeU9JKC5lDF4r367MYoXTbMcH9Y");
  myHeaders.append("x-api-key", "8d8ed390-541b-11eb-8ffc-114ff2da04bd");
  myHeaders.append("Content-Type", "application/json");
  var raw = JSON.stringify(data);
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };
  return fetch("https://cors-anywhere.herokuapp.com/https://rentals.thehousemonk.com/integration/frontapp/file-upload-url", requestOptions)
    .then(response => response.json())
    .then(result => {
      return result
    })
    .catch(error => {
      throw error
    });
}

const uploadFile = async (url, file) => {
  console.log('Uploading file.....')

  if (!url || !file) {
    throw new Error ('Required fields are missing at upload file.')
  }

  var requestOptions = {
    method: 'PUT',
    body: file,
    redirect: 'follow'
  };
  return fetch(url, requestOptions)
    .then(response => response)
    .then(result => {
      if (result.ok) {
        return result
      } else {
        console.log(result)
        throw new Error('AWS S3 upload error.')
      }
    })
    .catch(error => {
      throw error
    });
}

const createTicket = async (data) => {
  console.log('Creating ticket.....')

  console.log(data)

  if ( !data.ticketCategory || !data.ticketBelongsToRefId || !data.raisedBy) {
    throw new Error ('Required fields are missing at create ticket.')
  }

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmM2NDYwNGZkYzY4NjVjMWI4ZWIwYiIsIm5hbWUiOiJDb3ZlJ3MgZnJvbnRhcHAgdG9rZW4iLCJ1dWlkIjoiOGQ4ZWQzOTAtNTQxYi0xMWViLThmZmMtMTE0ZmYyZGEwNGJkIiwib3JnYW5pemF0aW9uIjoiNWU5ODBmM2ZkMGZmZWU3Zjc5MjIwMjBiIiwidWlkIjoiNWZjODg0OWE2N2I2ZDAzMTdmMTc4ZWY4IiwiaWF0IjoxNjE4NDA2OTcxLCJleHAiOjE2MjYxODI5NzF9.ZkOWHLwRNAC1hS3VKeU9JKC5lDF4r367MYoXTbMcH9Y");
  myHeaders.append("x-api-key", "8d8ed390-541b-11eb-8ffc-114ff2da04bd");
  myHeaders.append("Content-Type", "application/json");
  var raw = JSON.stringify(data);
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };
  return fetch("https://cors-anywhere.herokuapp.com/https://rentals.thehousemonk.com/integration/frontapp/ticket", requestOptions)
    .then(response => response.json())
    .then(result => {
      console.log(result)
      if (result.message === "Ticket created!") {
        return result
      } else {
        throw new Error ('Error while creating.')
      }
    })
    .catch(error => {
      throw error
    });
}

const getCategories = async (projectId) => {
  
  if (!projectId) {
    throw new Error('Project ID required.')
  }

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmM2NDYwNGZkYzY4NjVjMWI4ZWIwYiIsIm5hbWUiOiJDb3ZlJ3MgZnJvbnRhcHAgdG9rZW4iLCJ1dWlkIjoiOGQ4ZWQzOTAtNTQxYi0xMWViLThmZmMtMTE0ZmYyZGEwNGJkIiwib3JnYW5pemF0aW9uIjoiNWU5ODBmM2ZkMGZmZWU3Zjc5MjIwMjBiIiwidWlkIjoiNWZjODg0OWE2N2I2ZDAzMTdmMTc4ZWY4IiwiaWF0IjoxNjE4NDA2OTcxLCJleHAiOjE2MjYxODI5NzF9.ZkOWHLwRNAC1hS3VKeU9JKC5lDF4r367MYoXTbMcH9Y");
  myHeaders.append("x-api-key", "8d8ed390-541b-11eb-8ffc-114ff2da04bd");
  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };
  return fetch("https://cors-anywhere.herokuapp.com/https://rentals.thehousemonk.com/integration/frontapp/categories?project="+projectId+"&limit=20&skip=0", requestOptions)
    .then(response => response.json())
    .then(result => {
      if (result.data) {
        if (Array.isArray(result.data)) {
          return result.data
        } else {
          throw new Error ('Error in categories data.')
        } 
      } else {
        throw new Error ('Error in categories data.')
      }
    })
    .catch(error => {
      throw error
    });
}

const updateTicket = async (ticketId, data) => {
  // return "Ticket updated!";
  console.log('Updating ticket.....')

  // if ( !data.frontapp) {
  //   throw new Error ('Required fields are missing at update ticket.')
  // }

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmM2NDYwNGZkYzY4NjVjMWI4ZWIwYiIsIm5hbWUiOiJDb3ZlJ3MgZnJvbnRhcHAgdG9rZW4iLCJ1dWlkIjoiOGQ4ZWQzOTAtNTQxYi0xMWViLThmZmMtMTE0ZmYyZGEwNGJkIiwib3JnYW5pemF0aW9uIjoiNWU5ODBmM2ZkMGZmZWU3Zjc5MjIwMjBiIiwidWlkIjoiNWZjODg0OWE2N2I2ZDAzMTdmMTc4ZWY4IiwiaWF0IjoxNjE4NDA2OTcxLCJleHAiOjE2MjYxODI5NzF9.ZkOWHLwRNAC1hS3VKeU9JKC5lDF4r367MYoXTbMcH9Y");
  myHeaders.append("x-api-key", "8d8ed390-541b-11eb-8ffc-114ff2da04bd");
  myHeaders.append("Content-Type", "application/json");
  var raw = JSON.stringify(data);
  var requestOptions = {
    method: 'PUT',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };
  return fetch("https://cors-anywhere.herokuapp.com/https://rentals.thehousemonk.com/integration/frontapp/tickets/"+ticketId, requestOptions)
    .then(response => {
      console.log(response)
      return response.json()
    })
    .then(result => {
      if (result.message === "Ticket updated!") {
        return result
      } else {
        console.log(result)
        throw new Error ('Error while updating.')
      }
    })
    .catch(error => {
      throw error
    });
}

const getTicket = async (ticketId) => {
  // return "Ticket updated!";
  console.log('Fetching ticket.....')

  if ( !ticketId) {
    throw new Error ('Required fields are missing at fetch ticket.')
  }

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZmM2NDYwNGZkYzY4NjVjMWI4ZWIwYiIsIm5hbWUiOiJDb3ZlJ3MgZnJvbnRhcHAgdG9rZW4iLCJ1dWlkIjoiOGQ4ZWQzOTAtNTQxYi0xMWViLThmZmMtMTE0ZmYyZGEwNGJkIiwib3JnYW5pemF0aW9uIjoiNWU5ODBmM2ZkMGZmZWU3Zjc5MjIwMjBiIiwidWlkIjoiNWZjODg0OWE2N2I2ZDAzMTdmMTc4ZWY4IiwiaWF0IjoxNjE4NDA2OTcxLCJleHAiOjE2MjYxODI5NzF9.ZkOWHLwRNAC1hS3VKeU9JKC5lDF4r367MYoXTbMcH9Y");
  myHeaders.append("x-api-key", "8d8ed390-541b-11eb-8ffc-114ff2da04bd");
  myHeaders.append("Content-Type", "application/json");
  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };
  return fetch("https://cors-anywhere.herokuapp.com/https://rentals.thehousemonk.com/integration/frontapp/tickets/"+ticketId, requestOptions)
    .then(response => {
      console.log(response)
      return response.json()
    })
    .then(result => result)
    .catch(error => {
      throw error
    });
}

// Front code
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Front.contextUpdates.subscribe(context => {
  switch(context.type) {
    case 'noConversation':
      console.log('No conversation selected');
      error(true, 'Please select a conversation.')
      break;
    case 'singleConversation':
      console.log('Selected conversation:', context.conversation);
      const {
        id:conversationId = '',
        recipient = {},
        type = ''
      } = context.conversation

      const {
        handle = ''
      } = recipient

      if (type && handle) {
        loading(true)
        getHomes(type,handle)
          .then(result => {
            loading(false);
            // console.log(result)
            populateHomes(result, context.conversation)
          })
          .catch(err => {
            console.log(err);
            error(true, err.message);
          })
      } else {
        console.log(type, handle);
        error(true, 'Internal server error.');
      }
      break;
    case 'multiConversations':
      console.log('Multiple conversations selected', context.conversations);
      error(true, 'Only single conversation.')
      break;
    default:
      console.error(`Unsupported context type: ${context.type}`);
      error(true, 'Please select a conversation. (error)')
      break;
  }
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// jquery DOM code
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function error(show, errorMessage = 'Error') {
  if (show) {
    loading(false);
    $('.errorMsg').text(errorMessage);
    $('.errorDiv').show()
  } else {
    $('.errorMsg').text(errorMessage);
    $('.errorDiv').hide()
  }
}

function loading(show) {
  if (show) {
    $('.loaderDiv').show()
  } else {
    $('.loaderDiv').hide()
  } 
}

// getTicket('5e5e8a39bcd5402c5391f3a7');
// testing
getHomes('email', 'paolo.castro1221@gmail.com')
.then(result => {
  // console.log(result, " : RESULT")
  populateHomes(result, {})
})
.catch(err => {
  console.log(err);
  error(true, err.message);
})

function populateHomes(result, conversation) {
  error(false);
  $(".conversationId").text(conversation.id);
  $(".conversationBody").text(conversation.blurb);
  if (Array.isArray(result)) {
    console.log(result)
    if (result.length > 0) {
      $(".nav-homes").empty();
      $(".div-flatmates").empty();
      $(".createTicketHead").empty();
      for (let [index, item] of result.entries()) {
        const {
          projectName = '',
          block = '',
          floor = '',
          door = '',
          _id:homeId = '',
          occupancyStatus:homeStatus = 'vacant',
          tenants = [],
          tenantFirstName = '',
          tenantLastName = '',
          tenantId = '' 
        } = item || {}

        let parseStatus = homeStatus.split('_').join(' ')
        
        if (parseStatus.includes('occupied')) {
          parseStatus = 'occupied'
        }
        if (parseStatus.includes('move in pending')) {
          parseStatus = 'booked'
        }

        console.log(item)
        const homeName = projectName + ' ' + block + floor + ' ' + door

        populateTab(homeId, homeName, parseStatus, index);
        populateList(homeId, "div-flatmates", tenants);
        populateModal(item, "createTicketHead", tenants);
        populateUser(homeId, tenantFirstName, tenantLastName)
        if (index === 0) {
          displayUser(homeId)
          displayTable(homeId);
          displayCustom('div-flatmates', homeId);
          displayCustom('createTicketHead', homeId);
        }
      }
    } else {
      error(true, 'No tickets found.')
    }
  } else if (result === "user not found") {
    console.log('ERROR : ',result)
    error(true, result);
  } else {
    console.log(result)
    error(true, "Source is not supported.");
  }
}

function populateUser(id, firstName = '', lastName = '') {
  $('.currentUser').append(`<p class="text-primary m-0 font-weight-bold floatRight displayNone userName ${id}">${firstName + ' ' + lastName}</p>`)
}

function displayUser(id) {
  $('.currentUser').find('.userName').addClass('displayNone') 
  $('.currentUser').find('.'+id).removeClass('displayNone') 
}
/////////////////////////////////////////////////////////////////////////////////
// no error functions
function populateTab(homeId, homeName, homeStatus, index) {
  // cleanTabs
    
  if (index === 0) {
    $(".nav-homes").append(`
      <li class="nav-item textCenter width50">
        <a class="nav-link active" href="#"><span>${homeName}</span> <br> <span class="badge badge-light caps">${homeStatus}</span></a>
        <span class="homeId displayNone">${homeId}</span>
      </li>
    `);  
  } else {
    $(".nav-homes").append(`
      <li class="nav-item textCenter width50">
        <a class="nav-link" href="#"><span>${homeName}</span> <br> <span class="badge badge-light caps">${homeStatus}</span></a>
        <span class="homeId displayNone">${homeId}</span>
      </li>
    `);
  }
}

function populateList(homeId, parentClass, values) {
  const lis = []
  for (const item of values) {
    lis.push(
    `<li class="list-group-item">
        <div class="row align-items-center no-gutters">
            <div class="col mr-2">
                <h6 class="mb-0"><strong>${(item.firstName || '') + ' ' + (item.lastName || '')}</strong></h6>
            </div>
            <div class="col-auto">
                <div class="custom-control custom-checkbox"><span class="displayNone ccemails">${item.email}</span><input class="custom-control-input checkForward" type="checkbox" id="${item._id+homeId}check"><label class="custom-control-label" for="${item._id+homeId}check"></label></div>
            </div>
        </div>
    </li>`)
  }
  $("."+parentClass).append(`
    <div class="listFind">
    <ul class="list-group list-group-flush list-flatmates displayNone ${homeId}">
      ` + lis.join('') + 
    `</ul>
    <button type="button" class="btn btn-primary sendCC list-flatmates displayNone ${homeId}">Forward</button>
    </div>
    `);
}

function populateModal(data, parentClass, values) {
  let tenantOptions = '';

  for (let tenant of data.tenants || []) {
    tenantOptions = tenantOptions + `<option value="${tenant._id || ''}">${(tenant.firstName || '') + ' ' + (tenant.lastName || '')}</option>`
  } 

  if (!tenantOptions) {
    tenantOptions = '<option value=""></option>'
  }

  $("."+parentClass).append(`
    <div class="createTicketModal displayNone ${data._id}">
      <p class="text-primary m-0 font-weight-bold" style="float: left;">Tickets</p>
      <button type="button" class="btn btn-primary floatRight marginHeadBtn btn-moreModal" data-toggle="modal" data-target="#${data._id + 'create'}">
        Raise ticket
      </button>

      <!-- The Modal -->
      <div class="modal fade modalLoadCategory" id="${data._id + 'create'}">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
          
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Raise ticket</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
              <form action="/action_page.php" class="createTicketForm">
                <div class="form-group">
                  <label>Tenant :</label>
                  <select class="form-control userId" required>
                    `+tenantOptions+`
                  </select>
                  <br>
                  <label>Category :</label>
                  <select class="form-control category" required>
                    <option value=""></option>
                  </select>
                  <br>
                  <label>Description :</label>
                  <textarea class="form-control notes" rows="2"></textarea>
                  <br>
                  <label>Files :</label>
                  <div class="custom-file mb-3">
                    <input type="file" class="custom-file-input newFileInput">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                  </div>
                  <div class="newFiles">
                  </div>


                  <input type="text" class="homeId displayNone" value=${data._id} required>
                  <input type="text" class="projectId displayNone" value=${data.projectId || ''} required>
                </div>
                <button type="submit" class="btn btn-primary floatRight width100px">Raise</button>
              </form>

            </div>
          
            
          </div>
        </div>
      </div>
    </div>
  `);
}

// toggle active class
$("body").on("click", ".nav-homes li", function (event) {
  
  cleanCustom('list-flatmates');
  cleanCustom('createTicketModal');

  $(".nav-homes").find("a").removeClass("active")
  $(this).find("a").addClass("active")
  
  const homeId = $(this).find('.homeId').text();
  displayTable(homeId);
  displayCustom('div-flatmates', homeId);
  displayCustom('createTicketHead', homeId);
  displayUser(homeId)
});

// load categories 
$(document).on('show.bs.modal','.modalLoadCategory', function () {
  getCategories($(this).find('.projectId').val())
    .then(result => {
      $(this).find('.category').empty();      
      for (const item of result) {
        $(this).find('.category').append(`<option value="${item._id}">${item.name || 'Unknown'}</option>`)
      }
    })
    .catch(error => {
      $(this).find('.category').empty();
      $(this).find('.category').append(`<option value=""></option>`)
      console.log(error)
    })
})

// helper dom function
function cleanCustom(customClass) {
  $("." + customClass).addClass('displayNone');
}

// display table
function displayTable(homeId) {
  loading(true);
  getTickets(homeId)
    .then(result => {
      // console.log(result)
      loading(false);
      populateTable("ticketsTable", result.data, homeId)
    })
    .catch(err => {
      console.log(err)
      error(true, err.message)
    })
}

function populateTable(tableBodyClass, rowData, homeId) {
  // cleanTable
  $("."+tableBodyClass).empty();

  if (rowData.length === 0) {
    $("."+tableBodyClass).append(`<tr>
        <td>N/A</td>
        <td class="caps">N/A</td>
        <td class="caps">N/A</td>
        <td class="caps">N/A</td>
        <td class="caps">N/A</td>
      </tr>`);
  }

  for (const item of rowData) {
    const {
      ticketCategory = {},
      raisedBy = {},
      frontapp = {}
    } = item

    const {
      name:categoryName = '',
      categoryType = ''
    } = ticketCategory

    const {
      conversationId = ''
    } = frontapp

    const currentId = $('.conversationId').text();

    let conversationLink = `
      <div class="alert alert-light conversationLink textLeft divLink">
        <i class='fas fa-link' style='color:#858796; font-size:20px;'></i>
        <span class="linkHomeId ${'linkHomeId'+item._id} displayNone">${homeId}</span>
      </div>
    ` 

    if (conversationId) {
      conversationLink = `
        <div class="alert alert-light alert-dismissible textLeft divLink">
          <button type="button" class="close btnCustom buttonDeleteLink">&times;</button>
          <a class="aTag" target="_blank" href="${'https://app.frontapp.com/open/'+conversationId}"><i class='fas fa-link' style='color:#4CAF50; font-size:20px;'></i></a>
          <span class="linkHomeId ${'linkHomeId'+item._id} displayNone">${homeId}</span>
        </div>
      `
    }
    if (currentId && conversationId === currentId) {
      conversationLink = `
        <div class="alert alert-light alert-dismissible textLeft divLink">
          <button type="button" class="close btnCustom buttonDeleteLink">&times;</button>
          <a class="aTag" target="_blank" href="${'https://app.frontapp.com/open/'+conversationId}"><i class='fas fa-link' style='color:#4e73df; font-size:20px;'></i></a>
          <span class="linkHomeId ${'linkHomeId'+item._id} displayNone">${homeId}</span>
        </div>
      `
    }

    $("."+tableBodyClass).append(`<tr>
        <td>${item.uid || ''}</td>
        <td class="caps">${item.status || ''}</td>
        <td class="caps">${categoryName}</td>
        <td class="caps">${conversationLink}</td>
        <td>
          <button type="button" class="btn btn-basic btn-moreModal" data-toggle="modal" data-target="#${item._id}"><i class='fas fa-plus'></i></button>
          <div class="modal fade modalTicket" id="${item._id}">
            <div class="modal-dialog modal-sm modal-dialog-centered">
              <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">Ticket</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                </div>                
              </div>
            </div>
          </div>
        </td>
      </tr>`);

  }  
}

$(document).on('show.bs.modal','.modalTicket', function () {
  loading(true)
  $(this).find('.modal-body').empty();
  getTicket($(this).attr('id'))
    .then(result => {
      console.log(result, 'RESULT')
      loading(false)
      const {
        ticketCategory = {},
        raisedBy = {},
        files = []
      } = result

      const {
        name:categoryName = '',
        categoryType = '',
        _id:categoryId = ''
      } = ticketCategory

      const showFIles = []

      for (const file of files) {
        showFIles.push(`
          <div class="alert alert-warning alertCustom" style="padding: .75rem 1.25rem !important;">
            <a href="${file.aws_url}" target="_blank">${file.fileName}</a>
          </div>
        `)
      }

      $(this).find('.modal-content').empty();
      $(this).find('.modal-content').append(`
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Ticket #${result.uid || ''}</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body modalBodyPad">
          
          <h4>Details:</h4>
          <form action="/action_page.php" class="updateTicketForm">
            
            <input type="text" class="projectId displayNone" value="${result.project || ''}">
            <input type="text" class="ticketId displayNone" value="${result._id || ''}">
            <input type="text" class="homeId displayNone" value="${result.home || ''}">
            <input type="text" class="ifChanged displayNone" value="false">
            
            <table class="table table-borderless">
              <tbody>
                <tr>
                  <td class="bold">Category <span class="floatRight colorLight">|</span></td>
                  <td class="caps">
                    <select class="form-control editCategory" disabled>
                      <option value="${categoryId}">${categoryName}</option>                  
                    </select>                    
                  </td>
                </tr>
                <tr>
                  <td class="bold">Type <span class="floatRight colorLight">|</span></td>
                  <td class="caps">${categoryType}</td>
                </tr>
                <tr>
                  <td class="bold">Description <span class="floatRight colorLight">|</span></td>
                  <td>
                    <textarea class="form-control editDescription" rows="2" disabled>${result.notes || ''}</textarea>
                  </td>
                </tr>
                <tr>
                  <td class="bold">Raised by <span class="floatRight colorLight">|</span></td>
                  <td>${(raisedBy.firstName || '') + ' ' + (raisedBy.lastName || '')}</td>
                </tr>
                <tr>
                  <td class="bold">Status <span class="floatRight colorLight">|</span></td>
                  <td>                
                    <select class="form-control editStatus" disabled>
                      <option value="open" ${result.status == 'open' ? 'selected':''}>Open</option>
                      <option value="resolved" ${result.status == 'resolved' ? 'selected':''}>Resolved</option>
                      <option value="in-progress" ${result.status == 'in-progress' ? 'selected':''}>In Progress</option>
                      <option value="rejected" ${result.status == 'rejected' ? 'selected':''}>Rejected</option>
                      <option value="deleted" ${result.status == 'deleted' ? 'selected':''}>Deleted</option>                      
                    </select>                  
                  </td>
                </tr>
                <tr>
                  <td class="bold">Priority <span class="floatRight colorLight">|</span></td>
                  <td>
                    <select class="form-control editPriority" disabled>
                      <option value="low" ${result.priority == 'low' ? 'selected':''}>Low</option>
                      <option value="high" ${result.priority == 'high' ? 'selected':''}>High</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="bold">Date created <span class="floatRight colorLight">|</span></td>
                  <td>${result.createdAt ? showDate(result.createdAt) : ''}</td>
                </tr>
                <tr>
                  <td class="bold">Deadline <span class="floatRight colorLight">|</span></td>
                  <td>
                    <input type="date" class="form-control editDeadlineDate" value="${result.deadlineDate ? parseDate(result.deadlineDate) : ''}" disabled>
                  </td>
                </tr>
                <tr>
                  <td class="bold">Ticket details link <span class="floatRight colorLight">|</span></td>
                  <td><a class="aTag" target="_blank" href="${'https://rentals.thehousemonk.com/godview/#/ticket/view/' + (result._id || '')}">Ticket #${result.uid || ''}</a></td>
                </tr>
              </tbody>
            </table>
            <div class="textRight">
              <button type="button" class="btn btn-primary editFormEnable">Edit</button>
              <button type="submit" class="btn btn-primary displayNone editFormSubmit" disabled>Submit</button>
            </div>
            
          </form>
          ${showFIles ? `
            <div class="divFiles">
              <h4>Files:</h4>
              `+showFIles+`
            </div>
          `: ''}
        </div>            
      `);
    })
    .catch(error => {
      $(this).find('.modal-content').empty()
      console.log(error)
    })
})

$(document).on("click", ".editFormEnable", function(){
  loading(true)
  $(this).addClass('displayNone')
  $(this).parent().find('.editFormSubmit').prop('disabled', false)
  $(this).parent().find('.editFormSubmit').removeClass('displayNone')

  const categortId = $(this).parent().parent().find('.editCategory').find(":selected").val()
  
  getCategories($(this).parent().parent().find('.projectId').val())
  .then(result => {

    $(this).parent().parent().find('.editCategory').empty();      
    for (const item of result) {
      $(this).parent().parent().find('.editCategory').append(`<option value="${item._id}" ${item._id === categortId ? 'selected':''}>${item.name || 'Unknown'}</option>`)
    }
    loading(false)
  })
  .catch(error => {
    $(this).find('.category').empty();
    $(this).find('.category').append(`<option value=""></option>`)
    console.log(error)
  })

  $(this).parent().parent().find('select').prop('disabled', false)
  $(this).parent().parent().find('textarea').prop('disabled', false)
  $(this).parent().parent().find('input').prop('disabled', false)
})  

$(document).on('submit','.updateTicketForm',function(event){
  
  event.preventDefault()

  loading(true)

  const body = {
    notes: $(this).find('.editDescription').val(),
    ticketCategory: $(this).find('.editCategory').val(), // categoryId
    status: $(this).find('.editStatus').val(),
    priority: $(this).find('.editPriority').val()
  }

  if ($(this).find('.editDeadlineDate').val()) {
    body.deadlineDate = new Date($(this).find('.editDeadlineDate').val()) 
  }

  console.log(body)

  $(this).find('select').prop('disabled', true)
  $(this).find('textarea').prop('disabled', true)
  $(this).find('input').prop('disabled', true)
  const ticketId = $(this).find('.ticketId').val()
  updateTicket(ticketId, body)
    .then(result => {
      console.log(result, result.message === "Ticket updated!" ,'PPPPPPPPPPPPPPPPP')
      if (result.message === "Ticket updated!") {
        loading(false);
        // refresh tickets table
        $(this).find('.ifChanged').val("true")
        $(this).find('.editFormSubmit').addClass('displayNone')
        $(this).find('.editFormSubmit').prop('disabled', true)
        $(this).find('.editFormEnable').removeClass('displayNone')
      } else {
        console.log(result)
      }
    })
    .catch(error => console.log(error))

  return false
})

$(document).on('hidden.bs.modal','.modalTicket', function () {
  const ifChanged = $(this).find('.ifChanged').val()
  if (ifChanged === 'true') {
    const homeId = $(this).find('.homeId').val()
    displayTable(homeId)
  }
})

$(document).on("click", ".conversationLink", function(){

  const conversationId = $('.conversationId').text();
  if (conversationId) {
    const ticketId = $(this).parent().parent().find(".modal").attr('id');
    console.log('clicked', ticketId, conversationId)
    loading(true)
    updateTicket(ticketId, {frontapp:{status:"success", conversationId:conversationId}})
      .then(result => {
        console.log(result, result.message === "Ticket updated!" ,'PPPPPPPPPPPPPPPPP')
        if (result.message === "Ticket updated!") {
          loading(false);
          // refresh tickets table
          console.log(ticketId)
          console.log($(this).find('.linkHomeId').text(), 'lllllllllllllllllllll');
          displayTable($(this).find('.linkHomeId').text());
        } else {
          console.log(result)
        }
      })
      .catch(error => console.log(error))
  }
})

// display cusotom
function displayCustom(parentClass, homeId) {
  $('.'+parentClass).find('.'+homeId).removeClass('displayNone');
}

// modal bug fix
$(document).on('click', '.btn-moreModal', function (even) {
  // console.log($(this).data("target"))
  $($(this).data("target")).modal('show');
}) 



// uploadfile 
$(document).on('change','.newFileInput',function(){
  if($(this)[0] && $(this)[0].files[0] && $(this)[0].files[0].name){
    loading(true);
    console.log($(this)[0].files[0].name, ' : FILENAME')
    getSignedUrl({fileName: $(this)[0].files[0].name})
      .then(result => {
        console.log(result, ' : GETSIGNED URL RESULT.')
        if (result.url) {
          uploadFile(result.url, $(this)[0].files[0])
            .then(res => {
              console.log(res)
              loading(false);
              populateFiles($(this).parent().parent().find('.homeId').val(), result)
              $(this).val('')
            })
            .catch(error => console.log(error))
        } else {
          console.log(result)
        }
      })
      .catch(error => {
        console.log(error)
      })
  } 
})

function populateFiles(homeId, data) {
  $('#'+homeId+'create').find('.newFiles').append(`
    <div class="alert alert-warning newFile">
      <button type="button" class="close deleteFile" data-dismiss="alert">&times;</button>
      ${data.fileName}
      <input type="text" class="form-control displayNone success" value=${data.success}>
      <input type="text" class="form-control displayNone message" value=${data.message}>
      <input type="text" class="form-control displayNone url" value=${data.url}>
      <input type="text" class="form-control displayNone mimeType" value=${data.mimeType}>
      <span class="form-control displayNone fileName">${data.fileName}</span>
      <span class="form-control displayNone key">${data.key}</span>
    </div>
  `)
}

// delete uploaded file 
$(document).on('submit','.deleteFile',function(){
  $(this).parent().remove();
})


// create ticket submit
$(document).on('submit','.createTicketForm',function(){
  const newFiles = []
  $(this).find('.newFiles').find('.newFile').each(function () {
    newFiles.push({
      "success": $(this).find('.success').val() === 'true' ? true : false,
      "message": $(this).find('.message').val(),
      "url": $(this).find('.url').val(),
      "mimeType": $(this).find('.mimeType').val(),
      "fileName": $(this).find('.fileName').text(),
      "key": $(this).find('.key').text(),
    })
  })

  const data = {
      "ticketCategory": $(this).find('.category').val(), // categoryId
      "ticketBelongsToRefId": $(this).find('.homeId').val(), // homeId
      "notes": $(this).find('.notes').val(), // description
      "raisedBy": $(this).find('.userId').val(), // userId
      "ticketBelongsTo": "Home", // hardcoded
      "newFiles": newFiles, // files array
      "frontapp": {
        "status": "success",
        "conversationId":$('.conversationId').text(),
      }
  }
  console.log(data)
  loading(true);
  createTicket(data)
    .then(result => {
      console.log(result)
      if (result.message === "Ticket created!") {
        loading(false);
        // refresh tickets table
        displayTable($(this).find('.homeId').val());
        $('#'+$(this).find('.homeId').val()+'create').find('.close').click();
      } else {
        console.log(result)
      }
    })
    .catch(error => console.log(error))
  
  return false
});

// custom function
function parseDate(iSODateString) {
  date = new Date(iSODateString);
  year = date.getFullYear();
  month = date.getMonth()+1;
  dt = date.getDate();

  if (dt < 10) {
    dt = '0' + dt;
  }
  if (month < 10) {
    month = '0' + month;
  }

  return (year+'-' + month + '-'+dt);
}

function showDate(iSODateString) {
  date = new Date(iSODateString);
  year = date.getFullYear();
  month = date.getMonth()+1;
  dt = date.getDate();

  if (dt < 10) {
    dt = '0' + dt;
  }
  if (month < 10) {
    month = '0' + month;
  }

  return (dt +'-' + month + '-'+year);
}

$(document).on('click', '.sendCC', function(){
  console.log('Forward clicked.....')
  let emails = [];
  $(this).parent().find('.checkForward:checkbox:checked').each(function(){
    emails.push($(this).parent().find('.ccemails').text());
  })
  if (emails.length > 0) {
    forwardEmail(emails)
  } else {
    console.log('No tenant selected.')
  }
})

// forward email
async function forwardEmail(emails) {
  console.log('Sending email.......')
  const draft = await Front.createDraft({
    to: emails,
    content: {
      body: $('.conversationBody').text(),
      type: 'text'
    }
  });
}

$(document).on('click', '.buttonDeleteLink', function() {
  const ticketId = $(this).parent().parent().parent().find('.modal').attr('id')
  console.log('clicked', ticketId)
  console.log($(this).parent().find('.linkHomeId').text(), ' : homeId');
  const homeId = $(this).parent().find('.linkHomeId').text()
  loading(true)
  updateTicket(ticketId, {frontapp:{status:"failed", conversationId:''}})
    .then(result => {
      if (result.message === "Ticket updated!") {
        loading(false);
        // refresh tickets table
        displayTable(homeId);
      } else {
        console.log(result)
      }
    })
    .catch(error => console.log(error))
})

$(document).ready(function(){  

});